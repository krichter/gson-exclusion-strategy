package richtercloud.gson.exclusion.strategy;

import java.io.Serializable;

/**
 *
 * @author richter
 */
public class Superclass implements Serializable {
    private static final long serialVersionUID = 1L;
    private String myProperty;

    public Superclass() {
    }

    public Superclass(String myProperty) {
        this.myProperty = myProperty;
    }

    public String getMyProperty() {
        return myProperty;
    }

    public void setMyProperty(String myProperty) {
        this.myProperty = myProperty;
    }
}
