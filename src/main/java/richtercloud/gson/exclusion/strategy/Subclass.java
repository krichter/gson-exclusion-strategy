package richtercloud.gson.exclusion.strategy;

/**
 *
 * @author richter
 */
public class Subclass extends Superclass {
    private static final long serialVersionUID = 1L;
    private int someProperty;

    public Subclass() {
    }

    public Subclass(int someProperty, String myProperty) {
        super(myProperty);
        this.someProperty = someProperty;
    }

    public int getSomeProperty() {
        return someProperty;
    }

    public void setSomeProperty(int someProperty) {
        this.someProperty = someProperty;
    }
}
